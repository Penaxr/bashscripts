#!/bin/sh
# Allume les LEDS du clavier. 
# Ajouter ce Bash Script via un cron job avec l'option @reboot
# Pas oublier de "chmod +x" si executer via cron
xset led on
