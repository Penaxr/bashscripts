curl -s -w 'Testing Website Response Time for: %{url_effective}

Lookup Time:		%{time_namelookup}
Connect Time:		%{time_connect}
AppCon Time:		%{time_appconnect}
Redirect Time:		%{time_redirect}
Pre-transfer Time:	%{time_pretransfer}
Start-transfer Time:	%{time_starttransfer}

Total Time:		%{time_total}
' -o /dev/null https://simplementlinux.com
